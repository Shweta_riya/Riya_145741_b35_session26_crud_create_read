<?php
namespace App\BookTitle;
use PDO;
use App\Message\Message;
use App\Model\database as db;
use App\Utility\Utility;

//require_once("../../../../vendor/autoload.php");
class BookTitle extends db
{
    public $id;
    public $book_title;
    public $author_name;

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data = Null)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];

        }
        if (array_key_exists('book_title', $data)) {
            $this->book_title = $data['book_title'];

        }
        if (array_key_exists('author_name', $data)) {
            $this->author_name = $data['author_name'];

        }

    }

    public function store()
    {
        $arrData = array($this->book_title, $this->author_name);

        $sql = "Insert INTO book_title(book_title, author_name) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if ($result)
            Message::setMessage("Sucess!data has been inserted sucessfully");
        else
            Message::setMessage("Failure!data has not been inserted sucessfully");
        Utility::redirect('create.php');
    }// end of store method


    public function index($fetchMode = 'ASSOC')
    {

        $STH = $this->DBH->query('SELECT * from book_title ');

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode = 'ASSOC')
    {

        $sql='SELECT * from book_title WHERE id='.$this->id;
        $STH=$this->DBH->query($sql);
        //echo $sql;

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData = $STH->fetch();
        return $arrOneData;


    }// end of index();

}





































































































